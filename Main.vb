Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module PlaySlots
    
    Dim rand        As Random
    Dim seed        As Integer = 1
    Dim coins       As Integer = 1
    Dim maxTime     As Integer = 1
    Dim noteTime    As Integer = 1
    Dim numMachines As Integer = 1
    Dim wheels(,)   As String
    Dim wheelSize() As Integer
    
    Dim remTime     As Integer = 1
    Dim curCoins    As Integer = 1
    
    Dim ReadOnly PayoutTable() As Integer = { 1000, 200, 100, 50, 20, 10, 5 }
    
    Const MinCoins       As Integer = 100
    Const MaxCoins       As Integer = 10000
    Const MinMaxTime     As Integer = 100
    Const MaxMaxTime     As Integer = 10000
    Const MinNoteTime    As Integer = 2
    Const MaxNoteTime    As Integer = 10
    Const MinNumMachines As Integer = 3
    Const MaxNumMachines As Integer = 10
    Const MinWheelSize   As Integer = 10
    Const MaxWheelSize   As Integer = 30
    Const WheelSymbols   As String  = "AABBBBCCCCCDDDDDDEEEEEEFFFFFFFGGGGGGGG"
    
    Public Function quickPlay(machineNumber As Integer, times As Integer) As Integer
        If machineNumber < 0 OrElse machineNumber >= numMachines Then
            Throw New Exception(String.Format( _
                "quickPlay: IllegalArgument(machineNumber = {0})", _
                machineNumber))
        End If
        If times < 0 OrElse times > remTime OrElse times > curCoins Then
            Throw New Exception(String.Format( _
                "quickPlay: IllegalArgument(times = {0})", _
                times))
        End If
        remTime -= times
        quickPlay = CInt(DoPlay(machineNumber, times)(0))
    End Function
    
    Public Function notePlay(machineNumber As Integer, times As Integer) As String()
        If machineNumber < 0 OrElse machineNumber >= numMachines Then
            Throw New Exception(String.Format( _
                "notePlay: IllegalArgument(machineNumber = {0})", _
                machineNumber))
        End If
        If times < 0 OrElse noteTime * times > remTime OrElse times > curCoins Then
            Throw New Exception(String.Format( _
                "notePlay: IllegalArgument(times = {0})", _
                times))
        End If
        remTime -= noteTime * times
        notePlay = DoPlay(machineNumber, times)
    End Function
    
    Private Function DoPlay(machineNumber As Integer, times As Integer) As String()
        Dim ret(times) As String
        Dim n As Integer = wheelSize(machineNumber)
        Dim w0 As String = wheels(machineNumber, 0)
        Dim w1 As String = wheels(machineNumber, 1)
        Dim w2 As String = wheels(machineNumber, 2)
        Dim payout As Integer = 0
        curCoins -= times
        For i As Integer = 1 To times
            Dim s0 As Integer = rand.Next(n) + n
            Dim s1 As Integer = rand.Next(n) + n
            Dim s2 As Integer = rand.Next(n) + n
            ret(i) = w0(s0 - 1) + w1(s1 - 1) + w2(s2 - 1) _
                   + w0(s0 + 0) + w1(s1 + 0) + w2(s2 + 0) _
                   + w0(s0 + 1) + w1(s1 + 1) + w2(s2 + 1)
            If w0(s0) = w1(s1) AndAlso w0(s0) = w2(s2) Then
                Dim p As Integer = Asc(w0(s0)) - Asc("A")
                payout += PayoutTable(p)
            End If
        Next i
        curCoins += payout
        ret(0) = CStr(payout)
        doPlay = ret
    End Function
    
    Private Sub Shuffle(Of T)(rand As Random, arr() As T)
        For i As Integer = 0 To arr.Length - 2
            Dim j As Integer = rand.Next(i, arr.Length)
            Dim tmp As T = arr(i)
            arr(i) = arr(j)
            arr(j) = tmp
        Next i
    End Sub
    
    Private Function CalcEpectedPayoutRate(machine As Integer) As Double
        Dim symbols As String = "ABCDEFG"
        Dim n As Integer = wheelSize(machine)
        Dim count As Integer = 0
        For i As Integer = 0 To symbols.Length - 1
            Dim expect As Integer = 1
            For j As Integer = 0 To 2
                Dim tmp As Integer = 0
                For k As Integer = 0 To n - 1
                    If wheels(machine, j)(k) = symbols(i) Then
                        tmp += 1
                    End If
                Next k
                expect *= tmp
            Next j
            count += expect * PayoutTable(i)
        Next i
        CalcEpectedPayoutRate = CDbl(count) / CDbl(n * n * n)
    End Function
    
    Private Sub Generate()
        rand        = New Random(seed)
        coins       = rand.Next(MinCoins, MaxCoins + 1)
        maxTime     = rand.Next(MinMaxTime, MaxMaxTime + 1)
        noteTime    = rand.Next(MinNoteTime, MaxNoteTime + 1)
        numMachines = rand.Next(MinNumMachines, MaxNumMachines + 1)

        ReDim wheels(numMachines - 1, 2)
        ReDim wheelSize(numMachines - 1)
        For i As Integer = 0 To numMachines - 1
            Dim size As Integer = rand.Next(MinWheelSize, MaxWheelSize + 1)
            wheelSize(i) = size
            For j As Integer = 0 To 2
                Dim tmp As String = ""
                For k As Integer = 0 To size - 1
                    tmp += WheelSymbols(rand.Next(WheelSymbols.Length))
                Next k
                wheels(i, j) = tmp + tmp + tmp
            Next j
        Next i
        
        remTime = maxTime
        curCoins = coins
        
        Console.WriteLine("Seed = {0}", seed)
        Console.WriteLine()
        Console.WriteLine("Coins: {0}", coins)
        Console.WriteLine("Max Time: {0}", maxTime)
        Console.WriteLine("Note Time: {0}", noteTime)
        Console.WriteLine("Num Machines: {0}", numMachines)
        For i As Integer = 0 To numMachines - 1
            Console.WriteLine()
            Console.WriteLine("Machine {0}...", i)
            For j As Integer = 0 To 2
                Console.WriteLine("Wheel {0}: {1}", j, wheels(i, j).Substring(0, wheelSize(i)))
            Next j
            Dim rate As Double = CalcEpectedPayoutRate(i)
            Console.WriteLine("Expected payout rate: {0}", rate)
        Next i
        Console.WriteLine()
    End Sub
    
	Public Sub Main()
        Dim score As Double = 0.0
		Try
            ParseArgs()
            Generate()
            
            Console.SetError(Console.Out)
            
			Dim ps As New BrokenSlotMachines()
			Dim ret As Integer = ps.playSlots(coins, maxTime, noteTime, numMachines)
            
            Console.WriteLine()
			Console.WriteLine("ret = {0}", ret)
			Console.WriteLine("Final Coins: {0}", curCoins)
			Console.WriteLine("Rem Time: {0}", remTime)
			score = CDbl(curCoins) / CDbl(coins)
			
		Catch Ex As Exception
			Console.WriteLine(ex)
        Finally
            Console.WriteLine("Score = {0}", score)
            Console.WriteLine()
			Console.Out.Flush()
		End Try
	End Sub

    Private Sub ParseArgs()
        Dim args() As String = Environment.GetCommandLineArgs()
        Dim i As Integer = 1
        Do While i < args.Length
            Select Case args(i)
            Case "-seed"
                i += 1
                seed = CInt(args(i))
            End Select
            i += 1
        Loop
    End Sub
    
End Module