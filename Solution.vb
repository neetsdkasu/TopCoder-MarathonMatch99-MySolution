Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Double, Integer)
Imports Tp3 = System.Tuple(Of Double, Integer, Integer)

Public Class BrokenSlotMachines
	Dim rand As New Random(19831983)
	
    Dim ReadOnly PayoutTable() As Integer = { 1000, 200, 100, 50, 20, 10, 5 }

	Public Function playSlots(coins As Integer, maxTime As Integer, noteTime As Integer, numMachines As Integer) As Integer
		playSlots = 0
		Dim time0 As Integer = Environment.TickCount + 9800
		
		Console.Error.WriteLine("{0}, {1}", time0, rand.Next(0, 100))
		
		' Global.PlaySlots.notePlay(0, 1)
		' Global.PlaySlots.quickPlay(0, 1)
        
        Dim first As Integer = coins
        Dim all As Integer = numMachines
        Dim ch As Integer = 10
        
        Dim tms As Integer = noteTime * ch * numMachines
        If tms > coins OrElse tms * 100 > maxTime * 30 Then
            Do While numMachines > 0
                numMachines -= 1
                tms = noteTime * ch * numMachines
                If tms <= coins AndAlso tms * 100 <= maxTime * 30 Then
                    Exit Do
                End If
            Loop
            If numMachines < 1 Then
                numMachines = all
                Do While ch > 0
                    ch -= 1
                    tms = noteTime * ch * numMachines
                    If tms <= coins AndAlso tms * 100 <= maxTime * 30 Then
                        Exit Do
                    End If
                Loop
                If ch < 1 Then
                    Dim ys As New List(Of Tp3)()
                    For i As Integer = 3 To 9
                        For j As Integer = 2 To all - 1
                            ys.Add(New Tp3(-i * j, i, j))
                        Next j
                    Next i
                    ys.Sort()
                    For Each y As Tp3 In ys
                        tms = noteTime * y.Item2 * y.Item3
                        If tms <= coins AndAlso tms * 100 <= maxTime * 30 Then
                            ch = y.Item2
                            numMachines = y.Item3
                            Exit For
                        End If
                    Next y
                    If ch < 1 Then Exit Function
                End If
            End If
        End If
        
        tms = noteTime * ch * numMachines
        If tms * 100 < maxTime * 30 Then
            Dim tch As Integer = (maxTime * 30) \ (100 * noteTime * numMachines)
            tch = Math.Min(20, tch)
            For i As Integer = tch To ch Step -1
                tms = noteTime * i * numMachines
                If tms * 100 < coins * 10 Then
                    ch = i
                    Exit For
                End If
            Next i
        End If
        
        Dim symbols(numMachines - 1, 2, 6) As Integer
        Dim exepcted(numMachines - 1) As Double
        Dim cnts(numMachines - 1) As Integer
        Dim wins(numMachines - 1) As Integer
        
        For i As Integer = 0 To numMachines - 1
            coins -= ch
            maxTime -= ch * noteTime
            coins += Analyze(i, ch, cnts, wins, symbols, exepcted)
        Next i
        
        Dim ls(numMachines - 1) As Tp
        For i As Integer = 0 To numMachines - 1
            ls(i) = New Tp(-exepcted(i), i)
        Next i
        
        ' ch = 3
        ' tms = noteTime * ch
        ' For e As Integer = 0 To UBound(ls)
            ' Dim mx As Integer = ls(e).Item2
            ' If wins(mx) > 0 AndAlso 0.4 < exepcted(mx) AndAlso exepcted(mx) < 1.5 _
                    ' AndAlso tms * 4 < coins AndAlso tms * 4 < maxTime Then
                ' coins -= ch
                ' maxTime -= ch * noteTime
                ' coins += Analyze(mx, ch, cnts, wins, symbols, exepcted)
                ' ls(e) = New Tp(-exepcted(mx), mx)
            ' End If
        ' Next e

        Array.Sort(ls)

        ch = 5
        tms = noteTime * ch
        Dim bnd As Integer = UBound(ls) * 3 \ 5
        For e As Integer = 0 To numMachines - 1
            Dim mx As Integer = ls(e).Item2
            If cnts(mx) > 50 Then Continue For
            If (e <= bnd OrElse exepcted(mx) > 1.0) _
                    AndAlso tms < coins AndAlso tms < maxTime Then
                coins -= ch
                maxTime -= ch * noteTime
                coins += Analyze(mx, ch, cnts, wins, symbols, exepcted)
                ls(e) = New Tp(-exepcted(mx), mx)
            End If
        Next e
        
        Array.Sort(ls)
        ch = 10
        tms = noteTime * ch
        For e As Integer = 0 To UBound(ls) * 3 \ 5
            Dim mx As Integer = ls(e).Item2
            If cnts(mx) > 50 Then Continue For
            If exepcted(mx) > 0.8 AndAlso  tms < coins AndAlso tms < maxTime Then
                coins -= ch
                maxTime -= ch * noteTime
                coins += Analyze(mx, ch, cnts, wins, symbols, exepcted)
                ls(e) = New Tp(-exepcted(mx), mx)
            End If
        Next e
		
        Array.Sort(ls)

        ch = 7
        tms = noteTime * ch
        For i As Integer = 0 To Math.Min(1, numMachines - 1)
            Dim mx As Integer = ls(i).Item2
            If cnts(mx) > 50 Then Continue For
            If exepcted(mx) < 0.8 Then Continue For
            If tms < coins AndAlso tms < maxTime Then
                coins -= ch
                maxTime -= ch * noteTime
                coins += Analyze(mx, ch, cnts, wins, symbols, exepcted)
                ls(i) = New Tp(-exepcted(mx), mx)
            End If
        Next i
        
        ' For i As Integer = 0 To UBound(ls)
            ' Dim mx As Integer = ls(i).Item2
            ' If exepcted(mx) < 1.0 Then Continue For
            ' Dim ex As Double = exepcted(mx) + CDbl(wins(mx)) / CDbl(cnts(mx))
            ' ls(i) = New Tp(-ex, mx)
        ' Next i
        
        Array.Sort(ls)
        Dim mx2 As Integer = ls(0).Item2
        
        Console.Error.WriteLine("SEL({0}) -> {1} , wins {2}", _
            mx2, exepcted(mx2), CDbl(wins(mx2)) / CDbl(cnts(mx2)))
        If exepcted(mx2) < 1.1 Then Exit Function
        
        Dim limit1 As Integer = (coins * 60) \ 100
        Dim limit2 As Integer = (coins * 50) \ 100
        
        Do
            If coins <= limit1 OrElse maxTime <= 0 Then Exit Do
            coins -= 1
            maxTime -= 1
            coins += Global.PlaySlots.quickPlay(mx2, 1)
            limit1 = Math.Max(limit1, (coins * 30) \ 100)
        Loop
        
        If ls.Length > 1 AndAlso exepcted(ls(1).Item2) > 1.09 Then
            If cnts(mx2) < 50 Then
                mx2 = ls(1).Item2
                Console.Error.WriteLine("SEL({0}) -> {1} , wins {2}", _
                    mx2, exepcted(mx2), CDbl(wins(mx2)) / CDbl(cnts(mx2)))
            End If
        End If
        
        limit2 = Math.Max(limit2, (limit1 * 80) \ 100)
        
        If cnts(mx2) > 50 Then
            If exepcted(mx2) < 2.0 Then
                limit2 = (first * 20) \ 100
                If coins <= limit2 Then
                    limit2 = (coins * 80) \ 100
                End If
            Else
                limit2 = 0
            End If
        End If
        
        Do
            If coins <= limit2 OrElse maxTime <= 0 Then Exit Do
            coins -= 1
            maxTime -= 1
            coins += Global.PlaySlots.quickPlay(mx2, 1)
        Loop
        
	End Function
    
    Private Function Analyze( _
            i As Integer, _
            ch As Integer, _
            cnts() As Integer, _
            wins() As Integer, _
            symbols(,,) As Integer, _
            exepcted() As Double _
            ) As Integer
        Dim ret() As String = Global.PlaySlots.notePlay(i, ch)
        Dim win As Integer =  CInt(ret(0))
        Analyze = win
        wins(i) += win
        For j As Integer = 1 To UBound(ret)
            For x As Integer = 0 To ret(j).Length - 1
                Dim p As Integer = Asc(ret(j)(x)) - Asc("A")
                symbols(i, x Mod 3, p) += 1
            Next x
        Next j
        exepcted(i) = 0.0
        cnts(i) += 3 * ch
        For j As Integer = 0 To 6
            Dim tmp As Integer = PayoutTable(j)
            For k As Integer = 0 To 2
                tmp *= symbols(i, k, j)
            Next k
            exepcted(i) += CDbl(tmp) / CDbl(cnts(i) * cnts(i) * cnts(i))
        Next j
        Console.Error.WriteLine("EX({0}) -> {1} , wins {2}", _
            i, exepcted(i), CDbl(wins(i)) / CDbl(cnts(i)))
    End Function
    
End Class